package pis.hue2.server;

import java.util.ArrayList;
import java.util.List;

public class TeilnehmerListe {
    private List<ServerWorker> workerList=new ArrayList<ServerWorker>();

    public synchronized void add(ServerWorker worker){
        workerList.add(worker);
    }
    public synchronized void remove(ServerWorker worker){
        workerList.remove(worker);
    }

    public synchronized List<ServerWorker> getWorkerList() {
        return workerList;
    }
}
