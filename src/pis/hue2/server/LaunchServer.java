package pis.hue2.server;

import pis.hue2.common.ServerGUI;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Object of this class is a thread and represent server.
 * Field    :
 * -    serverPort in integer
 * -    List of ServerWorker
 * -    ServerGUI(Graphic User Interface)
 *
 * @author idham
 */
public class LaunchServer extends Thread {
    public ServerGUI serverGUI = new ServerGUI();
    private int serverPort;
    private TeilnehmerListe workerList;
    private ServerSocket serverSocket;
    private static boolean isConnected;

    /**
     * Constructor of class LaunchServer initializes field of this class.
     *
     * @param serverPort
     */
    public LaunchServer(int serverPort) {
        super();
        this.serverPort = serverPort;
        workerList= new TeilnehmerListe();

    }

    public void setServerSocket(ServerSocket serverSocket) {
        this.serverSocket=serverSocket;
    }

    /**
     * Main class to launch the GUI of Server(ServerGUI.class).
     *
     * @param args
     */
    public static void main(String[] args) {

        new Thread() {
            @Override
            public void run() {
                javafx.application.Application.launch(ServerGUI.class);
            }
        }.start();


    }

    /**
     * Setter of serverGUI of this Class
     *
     * @param serverGUI
     */

    public void setGUI(ServerGUI serverGUI) {
        this.serverGUI = serverGUI;
    }

    /**
     * @return List of ServerWorker
     */
    public  List<ServerWorker> getWorkerList() {
        return workerList.getWorkerList();
    }

    /**
     * remover of ServerWorker in the list
     *
     * @param serverWorker
     */
    public synchronized void removeWorker(ServerWorker serverWorker) {
        workerList.remove(serverWorker);
    }

    /**
     * Process :
     * 1.   Create ServerPort
     * 2.   In infinite while loop   :  i.   Create ClientSocket when a client connected.
     *                                  ii.  Create instance of Server Worker and add it to the list of serverwWrker.
     *                                  iii. Delegates work to ServerWorker.
     */
    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(serverPort);
            serverGUI.showMessage("Server start");
            isConnected=true;
            while(isConnected) {

                    Socket clientSocket = serverSocket.accept();
                    serverGUI.showMessage(clientSocket + "connected");
                    ServerWorker worker = new ServerWorker(this, clientSocket);
                    workerList.add(worker);
                    worker.start();



            }
        } catch (IOException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }
    }
    public void closeSocket() throws IOException {
        serverSocket.close();
    }

}
