package pis.hue2.server;

import pis.hue2.common.ClientGUI;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Object of this class is a thread and manage client from the Server-Side.
 * Field    :
 * -    server (LaunchServer)
 * -    clientsocket
 * -    outputstream
 * -    login in String
 */
public class ServerWorker extends Thread {
    private LaunchServer server;
    private Socket clientSocket;
    private OutputStream outputStream;
    private String login;


    /**
     * Constructor of this class initializes all of the fields of server worker with
     * the parameters.
     * @param server
     * @param clientSocket
     */
    public ServerWorker(LaunchServer server, Socket clientSocket) {
        this.server = server;
        this.clientSocket = clientSocket;
    }


    /**
     *
     * @return login
     */
    public String getLogin() {

        return this.login;
    }

    public void run() {
        try {
            handleClientSocket();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * this method handle what will happen when client send String to the server
     * @throws IOException
     */
    private void handleClientSocket() throws IOException {
        this.outputStream = clientSocket.getOutputStream();
        InputStream inputStream = clientSocket.getInputStream();


        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] tokens = line.split(":");

            if (tokens != null && tokens.length > 0) {
                String cmd = tokens[0].trim();

                if ("connect".equalsIgnoreCase(cmd)) {
                    handleConnect(outputStream, tokens, line);
                } else if ("disconnect:".equalsIgnoreCase(line)) {
                    handleDisconnect("disconnect:ok");
                } else if ("message".equalsIgnoreCase(cmd)) {
                    handleMessage(outputStream, tokens);
                } else {
                    System.err.println("INVALID COMMAND");
                    handleDisconnect("disconnect:invalid_command");
                }

            }
            outputStream.flush();
        }
        clientSocket.close();
    }

    /**
     * this method handles if the client types [connect:(userName)]
     * if username is empty, it will send an error message.
     * if the username is not allowed(in used, contains unallowed symbol), it will send disconnect message accordingly.
     * if the logged user is already maxed(3), it will send disconnect message accordingly.
     * else, it will handle the login.
     * @param outputStream
     * @param tokens
     * @param line
     * @throws IOException
     */

    private void handleConnect(OutputStream outputStream, String[] tokens, String line) throws IOException {
        if (tokens.length == 1 || tokens[1].trim().isEmpty()) {

            String emptyMsg = "ERROR:CANT_BE_EMPTY" + "\n";
            outputStream.write(emptyMsg.getBytes());
        } else {
            if (connectedUser(this.server.getWorkerList()) == 3) {
                outputStream.write("refused:too_many_user\n".getBytes());
                System.err.println("Error:too many user");
                server.serverGUI.showMessage("Client "+clientSocket+" can't login,too many logged user");
                server.removeWorker(this);
                clientSocket.close();
            } else if (!nameIsNotThere(this.server.getWorkerList(), tokens[1].trim())) {
                System.err.println("Error:name in use");
                outputStream.write("refused:name_is_used\n".getBytes());
                server.serverGUI.showMessage("Client "+clientSocket+" can't login,username is used");
                server.removeWorker(this);
                clientSocket.close();
            } else if (!isNameAllowed(line)) {
                System.err.println("Error: Name is not allowed for " + tokens[1]);
                outputStream.write("refused:invalid_name\n".getBytes());
                server.serverGUI.showMessage("Client "+clientSocket+" can't login,username is not allowed");
                server.removeWorker(this);
                clientSocket.close();
            } else {
                this.login = tokens[1].trim();
                server.serverGUI.showMessage("Client "+ clientSocket+" connected with login ["+login+"]");
                String connected = "connect:ok" + "\n";
                outputStream.write(connected.getBytes());
                broadcast(server.getWorkerList(), onlineMsg(server.getWorkerList()));
                server.serverGUI.showList(onlineMsg(server.getWorkerList()).split(":"));
                System.out.println(this.login + " is connected");


            }
        }
    }

    /**
     * with worker list this method will generate string of onlineusers
     * @param workerList
     * @return namelist of onlineusers
     */
    private String onlineMsg(List<ServerWorker> workerList) {
        String onlineMsg = "namelist";
        for (ServerWorker worker : server.getWorkerList()) {
            if (worker.getLogin() != null) {

                onlineMsg = onlineMsg + ":" + worker.getLogin();

            } else {
                continue;
            }
        }
        return onlineMsg + "\n";
    }

    /**
     * this method is used to broadcast message to all user that is logged in in the server.
     * @param workerList
     * @param msg
     * @throws IOException
     */
    public void broadcast(List<ServerWorker> workerList, String msg) throws IOException {
        for (ServerWorker worker : server.getWorkerList()) {

            worker.send(msg + "\n");

        }
    }

    /**
     * handle thisconnect and send string[errormsg] to the client
     * @param errormsg
     * @throws IOException
     */
    public void handleDisconnect(String errormsg) throws IOException {

            server.removeWorker(this);
            if (this.login != null) {
                server.serverGUI.showMessage("[" + login + "]" + "logged out");
            }
            String offlineMsg = "namelist";
            for (ServerWorker worker : server.getWorkerList()) {
                if (worker.getLogin() != null) {

                    offlineMsg = offlineMsg + ":" + worker.getLogin();

                }
            }
            server.serverGUI.showList(offlineMsg.split(":"));
            broadcast(server.getWorkerList(), offlineMsg);
            outputStream.write(errormsg.getBytes());
            clientSocket.close();

    }

    /**
     * write with the outputstream of client string[msg]
     * @param msg
     * @throws IOException
     */

    public void send(String msg) throws IOException {
        if (this.login != null) {
            outputStream.write(msg.getBytes());
        }
    }

    /**
     * this method handles if the client types [message:(msg)]
     * if the client is not logged in, it will send an error message
     * else, it will handle the message and send it all across the logged client.
     * @param outputStream
     * @param tokens
     * @throws IOException
     */

    private void handleMessage(OutputStream outputStream, String[] tokens) throws IOException {
        if (this.login == null) {
            outputStream.write("ERROR:NOT LOGGED IN \n".getBytes());
        } else {
            String msg = "message:" + this.login + ":" + tokens[1] + "\n";
            server.serverGUI.showMessage("["+login+"] send ["+tokens[1]+"]");
            for (ServerWorker worker : server.getWorkerList()) {
                worker.send(msg);


            }
        }
    }

    /**
     * this method counts the number of logged user
     * @param workerList
     * @return
     */
    private int connectedUser(List<ServerWorker> workerList) {
        int connected = 0;
        for (ServerWorker worker : workerList) {
            if (worker.getLogin() != null) {
                connected++;
            }
        }
        return connected;
    }

    /**
     * this method checks if the login is already listed in the server
     * @param workerList
     * @param name
     * @return
     */

    private boolean nameIsNotThere(List<ServerWorker> workerList, String name) {
        if (workerList.isEmpty()) {
            return true;
        } else {
            for (ServerWorker worker : workerList) {
                if (worker.getLogin() == null) {
                    continue;
                } else {
                    if (worker.getLogin().equalsIgnoreCase(name)) {
                        return false;
                    }
                }
            }
        }
        return true;


    }

    /**
     * checks if the name is allowed
     * not allowed  :
     * -    contains[:].
     * -    longer than 30 symbols.
     * @param line
     * @return
     */
    private boolean isNameAllowed(String line) {
        int size = line.split(":").length;
        if (size == 2) {
            if (line.split(":")[1].length() <= 30) {
                return true;
            }
        }
        return false;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }
}

