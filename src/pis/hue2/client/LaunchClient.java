package pis.hue2.client;

import pis.hue2.common.ClientGUI;

import java.io.*;
import java.net.Socket;

/**
 * Object of this class represent each of the client.
 * Field :
 *  -   serverPort in integer
 *  -   serverHost in string
 *  -   socket of client
 *  -   outputstream
 *  -   inputstream
 *  -   printwriter
 *  -   bufferedwriter.
 *
 * @author idham
 */
public class LaunchClient {
    private int serverPort;
    private String serverHost;
    private Socket client;
    private InputStream inputStream;
    private OutputStream outputStream;
    private PrintWriter writer;
    private BufferedReader reader;

    /**
     * Constructor of class LaunchClient initializes field of this class.
     * @param serverHost
     * @param serverPort
     * @throws IOException
     *
     */
    public LaunchClient(String serverHost, int serverPort) throws IOException {
        this.serverPort = serverPort;
        this.serverHost = serverHost;

        client = new Socket(serverHost, serverPort);
        inputStream = client.getInputStream();
        outputStream = client.getOutputStream();
        writer = new PrintWriter(outputStream, true);
        reader = new BufferedReader(new InputStreamReader(inputStream));

    }
    /**
     * @param msg
     */
    public void sendToServer(String msg) {
        writer.println(msg);
    }

    /**
     * @return Reader of this LaunchClient class to be used in the other class.
     */
    public BufferedReader getReader() {
        return this.reader;
    }

    /**
     * Main class to launch the GUI of Client(ClientGUI.class).
     * @param args
     */
    public static void main(String[] args) {
        new Thread() {
            @Override
            public void run() {
                javafx.application.Application.launch(ClientGUI.class);
            }
        }.start();

    }
    public Socket getSocket(){
        return this.client;
    }

}
