package pis.hue2.common;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pis.hue2.client.LaunchClient;
import pis.hue2.server.ServerWorker;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Object of this class represent the Graphic user Interface of a client
 * Field    :
 * -    LaunchClient client
 */
public class ClientGUI extends Application {
    LaunchClient client;
    boolean isConnected;
    TextArea chatArea;

    @Override
    public void start(Stage primaryStage) throws Exception {


        //-----------------------------------------------------------------//
        primaryStage.setResizable(false);
        HBox root = new HBox();

        Scene scene = new Scene(root, 500, 300);

        TextField host= new TextField();
        host.setPromptText("host");
        TextField port= new TextField();
        port.setPromptText("port");
        Button connect= new Button("CONNECT");
        ListView onlineUser = new ListView();
        chatArea = new TextArea();
        chatArea.setEditable(false);
        chatArea.setPromptText("Connect with format connect:<UserName> ");


        TextField input = new TextField();
        Button sendButton = new Button("send");

        VBox left = new VBox();
        left.setAlignment(Pos.CENTER);
        left.setFillWidth(true);
        HBox userInput = new HBox();

        //-----------------------------------------------------------------//
        root.getChildren().add(left);
        root.getChildren().add(onlineUser);

        left.getChildren().addAll(host,port,connect);
        left.getChildren().add(new Label());
        left.getChildren().add(chatArea);
        left.getChildren().add(userInput);


        userInput.getChildren().add(input);
        userInput.getChildren().add(sendButton);
        //-----------------------------------------------------------------//

        sendButton.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * Button sendButton send the text in textfield input to the Server in which the client is connected.
             * @param event
             */
            @Override
            public void handle(ActionEvent event) {
                String in = input.getText();
                client.sendToServer(in);
                input.setText(null);

            }
        });
        input.setOnKeyPressed(new EventHandler<KeyEvent>() {
            /**
             * if focused to textfield, pressing KEY[ENTER] will simulate pressing the sendButton
             * @param ke
             */
            @Override
            public void handle(KeyEvent ke) {
                if (ke.getCode().equals(KeyCode.ENTER)) {
                    sendButton.fire();
                }
            }
        });
        connect.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event)  {
                try {
                    client = new LaunchClient(host.getText(), Integer.parseInt(port.getText()));
                    Thread fromServer = new Thread(new Runnable() {
                        /**
                         * Thread fromServer runs in the background to fetch data that was sent from
                         * server to the reader of the client and handle accordingly.
                         */
                        @Override
                        public void run() {

                            try {
                                String line;
                                while ((line = client.getReader().readLine()) != null) {
                                    String[] tokens = line.split(":");
                                    if (tokens != null && tokens.length > 0) {
                                        String cmd = tokens[0];
                                        if ("disconnect".equalsIgnoreCase(cmd) || "refused".equalsIgnoreCase(cmd)) {
                                            chatArea.appendText(line + "\n");
                                            try {
                                                TimeUnit.SECONDS.sleep(1);
                                            } catch (InterruptedException e) {
                                                e.printStackTrace();
                                            }

                                            System.exit(0);
                                        } else if ("namelist".equalsIgnoreCase(cmd)) {

                                            onlineUser.getItems().setAll(tokens);

                                        } else if ("message".equalsIgnoreCase(cmd)) {
                                            String getMessage = tokens[1] + ":" + tokens[2];
                                            chatArea.appendText(getMessage + "\n");
                                        } else {
                                            chatArea.appendText(line + "\n");
                                        }
                                    }

                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                                System.exit(0);
                            }
                        }

                    });


                    fromServer.start();


                } catch (IOException e) {
                    e.printStackTrace();
                    chatArea.appendText("Can't connect \n");

                }

            }
        });

        //-----------------------------------------------------------------//
        primaryStage.setTitle("Client");
        primaryStage.setScene(scene);
        primaryStage.show();


    }
    public void killClient(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                System.exit(0);
            }
        });
    }


}
