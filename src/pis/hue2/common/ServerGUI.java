package pis.hue2.common;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import pis.hue2.server.LaunchServer;
import pis.hue2.server.ServerWorker;

import java.io.IOException;
import java.net.ServerSocket;


/**
 *Object of this class represent the Graphic user Interface of a Server
 * Field    :
 * -    server (LaunchServer)
 *
 */

public class ServerGUI extends Application {

    TextArea action = new TextArea();
    ListView listView = new ListView();
    private LaunchServer server;
    boolean isAsleep;

    public static void main(String[] args) {
        launch(args);
    }


    @Override
    public void start(Stage primaryStage) {
        //-----------------------------------------------------------------//

        VBox root = new VBox();
        Scene scene = new Scene(root, 500, 300);

        Label status = new Label("status     :       disconnected");
        Button connectButton = new Button("connect");
        Button disconnectButton = new Button("disconnect");
        action.setEditable(false);
        Button close= new Button("close");

        HBox serverInterface = new HBox();
        serverInterface.getChildren().addAll(action, listView);

        root.getChildren().addAll(connectButton, serverInterface, status, disconnectButton,close);
        //-----------------------------------------------------------------//
        connectButton.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * if connectButton is pressed, the status label will be changed
             * @param event
             */
            @Override
            public void handle(ActionEvent event) {
                connectButton.setDisable(true);
                disconnectButton.setDisable(false);
                status.setText("status     :       connected");
                action.setText(null);
                server= new LaunchServer(9999);
                server.setGUI(ServerGUI.this);
                server.start();

            }
        });
        disconnectButton.setOnAction(new EventHandler<ActionEvent>() {
            /**
             * if disconnectButton is pressed, it will exit the program
             * @param event
             */
            @Override
            public void handle(ActionEvent event) {
                connectButton.setDisable(false);
                disconnectButton.setDisable(true);
                status.setText("status     :       disconnected");
                for(ServerWorker worker : server.getWorkerList()){
                    try {
                        worker.getOutputStream().write("disconnect:server is down\n".getBytes());
                        worker.getClientSocket().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    server.closeSocket();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                server.interrupt();
                action.setText("Server Disconnected");
                String []nullstring= new String[0];
                ServerGUI.this.showList(nullstring);
            }

        });
        close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.exit(0);
            }
        });
        //-----------------------------------------------------------------//
        primaryStage.setTitle("Server");
        primaryStage.setScene(scene);
        primaryStage.show();


    }

    /**
     * Platform.runLater thread,
     * receive msg from Server and append it to the text area.
     * @param msg
     */

    public void showMessage(String msg) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                action.appendText(msg + "\n");
            }
        });
    }

    /**
     * Platform.runLater thread,
     * receive array of Strings and update the list in GUI accordingly.
     * @param onlineUser
     */

    public void showList(String[] onlineUser) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                listView.getItems().setAll(onlineUser);
            }
        });
    }
}
